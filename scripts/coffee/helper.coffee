export getById = (id) ->
  return document.getElementById(id)

export getByClass = (className) ->
  return document.getElementsByClassName(className)

export hideMainScreenContents = () ->
  for element in getById('content').children
    element.style.display = "none"