import * as $ from "./helper"
import * as menu from "./menu"
import { timeline } from "./timeline"
import { background } from "./background"
import { lazyload } from "./lazyload"

timeout = 100
pollForP5 = () ->
  setTimeout () ->
    timeout--
    if p5?
      if screen.width >= 600
        new p5(timeline, $.getById('timeline'))
        $.getById('timeline').firstChild.style.opacity = 1
      if screen.width >= 500
        new p5(background, $.getById('background'))
        $.getById('background').firstChild.style.opacity = 1
    else if timeout > 0
      pollForP5()
  , 100

document.onreadystatechange = () ->
  if document.readyState is "complete"
    lazyload()
    for link in $.getById('sliderLinks').children
      name = link.id.split('Link')[0]
      link.addEventListener 'click', (event) ->
        event.stopImmediatePropagation()
        menu.menuInteraction(this.id)
      list = $.getById(name + 'List')
      if list?
        for item in list.children
          item.addEventListener 'click', (event) ->
            event.stopImmediatePropagation()
            menu.menuInteraction(this.id)
    for block in $.getByClass('block__sub-block')
      if block.firstChild.id.indexOf('CVLink') != -1
        block.firstChild.addEventListener 'click', (event) ->
          menu.menuInteraction(this.id.split('CVLink')[0] + 'Link')

    $.getById('contactLink').addEventListener 'click', (event) ->
      event.stopImmediatePropagation()
      $.getById('contact').style.display = 'flex'

    $.getById('contactFooterLink').addEventListener 'click', (event) ->
      event.stopImmediatePropagation()
      $.getById('contact').style.display = 'flex'

    $.getById('contactAboutMeLink').addEventListener 'click', (event) ->
      event.stopImmediatePropagation()
      $.getById('contact').style.display = 'flex'

    $.getById('closeContact').addEventListener 'click', (event) ->
      event.stopImmediatePropagation()
      $.getById('contact').style.display = 'none'

    $.hideMainScreenContents()
    menu.hideSliderLists()
    menu.menuInteraction('cvLink')
    $.getByClass('main')[0].style.opacity = '1'
    pollForP5()
    if $.getById('popup')?
      window.setTimeout () ->
        $.getById('popup').style.top = '-100px'
      , 1400