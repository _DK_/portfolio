import * as $ from './helper'
import * as data from './timelineData'
import * as menu from './menu'

export timeline = (p) ->
  hoverBox = []
  blocks = []
  yearLabels = []
  yearLines = []
  canvasDiv = undefined

  months = 96
  yearCount = 2011
  textSize = 16
  cols = undefined
  offsetLeft = undefined

  p.setup = () ->
    canvasDiv = $.getById('timeline')
    width = canvasDiv.offsetWidth
    height = canvasDiv.offsetHeight
    cols = p.int(width / months)
    offsetLeft = (width - (cols * months)) / 2
    p.createCanvas(width, height)
    p.frameRate(8)

    makeTimelineBlock = (list, section, top = true) ->
      multiplier = if top then 1.7 else 3.6

      for i in list
        for j in i.dates
          date = j
          x = offsetLeft + (date[0] * cols)
          y = textSize * multiplier
          w = (date[1] - date[0]) * cols
          h = textSize * 1.7

          unless top
            y++

          blocks.push({id: i.id, name: i.name, color: i.color, date: date, x: x, y: y, w: w, h: h, top: top})

    makeTimelineBlock(data.employers, 'employers')
    makeTimelineBlock(data.educators, 'education', false)

    for i in [0..months]
      if (i + 1) % 12 == 0 or i == 0
        yearLines.push({x1: offsetLeft + (i * cols), y1: textSize * 1.6, x2: offsetLeft + (i * cols), y2: height - (textSize * 1.4)})
        yearLabels.push({text: yearCount, x: offsetLeft + (i * cols), y: height - (textSize / 2)})
        yearCount++

  p.draw = () ->
    width = canvasDiv.offsetWidth
    height = canvasDiv.offsetHeight

    p.clear()

    for b in blocks
      p.strokeWeight(0)
      if p.mouseX > b.x and p.mouseX < b.x + b.w and p.mouseY > b.y and p.mouseY < b.y + b.h
        p.fill(255)
        p.cursor(p.HAND)
        hoverBox = [b.x, b.y, b.w, b.h]
        p.textAlign(p.LEFT, p.LEFT)
        p.text(b.name, b.x, textSize)
        p.strokeWeight(2)
        if p.mouseIsPressed
          unless $.getById(b.id + 'Link').firstChild.classList.contains('selectedSlideout')
            menu.menuInteraction(b.id + 'Link')

      ele = $.getById(b.id)
      if ele? and ele.style.display == 'block'
        p.strokeWeight(2)

      p.fill(b.color)
      p.rect(b.x, b.y, b.w, b.h)

    if hoverBox.length == 4
      unless p.mouseX > hoverBox[0] and p.mouseX < hoverBox[0] + hoverBox[2] and p.mouseY > hoverBox[1] and p.mouseY < hoverBox[1] + hoverBox[3]
        p.cursor(p.ARROW)
        hoverBox = []

    p.stroke(255)
    p.strokeWeight(0)
    p.fill(255)
    p.textSize(textSize)

    p.strokeWeight(3)
    for l in yearLines
      p.line(l.x1, l.y1, l.x2, l.y2)

    p.strokeWeight(1)
    for i in [0...yearLabels.length]
      if i == 0
        p.textAlign(p.LEFT, p.LEFT)
      else if i == yearLabels.length - 1
        p.textAlign(p.RIGHT, p.RIGHT)
      else
        p.textAlign(p.CENTER, p.CENTER)
      p.text(yearLabels[i].text, yearLabels[i].x, yearLabels[i].y)

    p.strokeWeight(3)
    p.line(offsetLeft, height - (textSize * 2), offsetLeft + ((months - 1) * cols), height - (textSize * 2))