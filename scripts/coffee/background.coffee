import * as $ from "./helper"

export background = (p) ->
  canvasDiv = $.getById('background')
  width = canvasDiv.offsetWidth
  height = canvasDiv.offsetHeight
  points = []

  p.setup = () ->
    p.createCanvas(width, height)
    p.frameRate(20)
    noNodes = (width + height) / 50
    for i in [0..noNodes]
      points[i] = [p.random(width), p.random(height), p.random(-1, 1), p.random(-1, 1)]

  p.draw = () ->
    width = canvasDiv.offsetWidth
    height = canvasDiv.offsetHeight
    p.clear()
    for d in points
      p.strokeWeight(0)
      p.stroke(200)
      p.point(d[0], d[1])
      d[0] += d[2]
      d[1] += d[3]
      if d[0] < 0
        d[0] = width
      if d[0] > width
        d[0] = 0
      if d[1] < 0
        d[1] = height
      if d[1] > height
        d[1] = 0

      close = []

      nearby = []
      for n in points
        if d != n
          if Math.abs(d[0] - n[0]) < 60 and Math.abs(d[1] - n[1]) < 60
            p.strokeWeight(1)
            p.stroke(100)
            p.line(d[0], d[1], n[0], n[1])
            nearby.push(n)

      if nearby.length > 1
        nearby.push(d)
        p.strokeWeight(0)
        color = p.color(255)
        color._array[3] = 0.2
        p.fill(color)
        p.beginShape()
        for n in nearby
          p.vertex(n[0], n[1])
        p.endShape()