import * as $ from './helper'
extended = false
last = undefined
timeout = 300
mobile = window.matchMedia("screen and (max-width: 800px)")

slideout = undefined
nav = undefined

selectedNav = undefined
selectedSlideout = undefined

export hideSliderLists = () ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  for list in slideout.children
    list.style.display = 'none'

switchNavFocus = (focus) ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  ele = $.getById(focus)
  if selectedNav?
    selectedNav.classList.remove('menu__link_selected')
  ele.classList.add('menu__link_selected')
  selectedNav = ele

switchSliderFocus = (focus) ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  ele = $.getById(focus)
  if selectedSlideout?
    selectedSlideout.classList.remove('slideout__item_selected')
  if ele.firstChild.classList?
    ele.firstChild.classList.add('slideout__item_selected')
    selectedSlideout = ele.firstChild
  else
    ele.classList.add('slideout__item_selected')
    selectedSlideout = ele

showSlider = (listToShow) ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  unless extended
    ele = $.getById(listToShow)
    if !mobile.matches
      ele.style.display = 'block'
      navWidth = nav.scrollWidth
      slideout.style.left = navWidth + "px"
    else
      ele.style.display = 'flex'
      navHeight = nav.scrollHeight
      slideoutHeight = slideout.scrollHeight
      eleHeight = ele.firstChild.scrollHeight
      top = navHeight - (slideoutHeight - eleHeight) - 1
      slideout.style.top = top + "px"
    extended = true

hideSlider = () ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  if extended
    if !mobile.matches
      slideout.style.left = "0px"
    else
      top = nav.scrollHeight - slideout.scrollHeight
      slideout.style.top = top + "px"
    window.setTimeout () ->
      hideSliderLists()
    , timeout
    extended = false

sliderSwitch = (listToShow) ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  if extended
    if !mobile.matches
      slideout.style.left = "0px"
    else
      top = nav.scrollHeight - slideout.scrollHeight
      slideout.style.top = top + "px"
    window.setTimeout () ->
      hideSliderLists()
      ele = $.getById(listToShow)
      if !mobile.matches
        ele.style.display = 'block'
        navWidth = nav.scrollWidth
        slideout.style.left = navWidth + "px"
      else
        ele.style.display = 'flex'
        navHeight = nav.scrollHeight
        slideoutHeight = slideout.scrollHeight
        eleHeight = ele.firstChild.scrollHeight
        top = navHeight - (slideoutHeight - eleHeight) - 1
        slideout.style.top = top + "px"
    , timeout

slide = (listToShow) ->
  if $.getById(listToShow)?
    unless extended
      showSlider(listToShow)
    else
      if last == listToShow
        hideSlider()
      else
        sliderSwitch(listToShow)
    last = listToShow

export menuInteraction = (itemClicked) ->
  if slideout == undefined
    slideout = $.getById('slideout')
  if nav == undefined
    nav = $.getById('nav')
  if itemClicked.indexOf('Link')?
    eleClicked = $.getById(itemClicked)
    if eleClicked.parentNode.id is "sliderLinks"
      switchNavFocus(itemClicked)
      listToShow = itemClicked.split('Link')[0] + 'List'
      slide(listToShow)
    else
      navParent = eleClicked.parentNode.id.split('List')[0] + 'Link'
      displayItem = $.getById(itemClicked.split('Link')[0])
      switchNavFocus(navParent)
      switchSliderFocus(itemClicked)
      $.hideMainScreenContents()
      if displayItem?
        displayItem.style.display = "block"
      hideSlider()