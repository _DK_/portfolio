export employers = [
  {
    name: "Planto",
    id: "planto",
    color: [26, 225, 183],
    dates: [
      [91, 93]
    ],
  },
  {
    name: "Sophos",
    id: "sophos",
    color: [5, 91, 201],
    dates: [
      [79, 91]
    ],
  },
  {
    name: "Unitemps",
    id: "unitemps",
    color: [211, 17, 36],
    dates: [
      [74, 76]
    ],
  },
  {
    name: "Zeta",
    id: "zeta",
    color: [210, 162, 42],
    dates: [
      [51, 57],
      [60, 61],
      [65, 69],
      [72, 73]
    ],
  },
  {
    name: "Bagwell Farm Touring Park",
    id: "bagwell",
    color: [44, 182, 15],
    dates: [
      [7, 9],
      [19, 21],
      [30, 32],
      [43, 45]
    ],
  },
  {
    name: "Upton Glen",
    id: "uptonglen",
    color: [89, 162, 210],
    dates: [
      [39, 42]
    ],
  }
]

export educators = [
  {
    name: "Thomas Hardye School",
    id: "ths",
    color: [244, 23, 4],
    dates: [
      [0, 7],
      [9, 19],
      [21, 30]
    ],
  },
  {
    name: "Bournemouth & Poole College",
    id: "bpc",
    color: [2, 82, 155],
    dates: [
      [32, 43],
      [45, 54]
    ],
  },
  {
    name: "University of Surrey",
    id: "uos",
    color: [0, 40, 104],
    dates: [
      [57, 66],
      [69, 78],
      [81, 90],
      [93, 95]
    ],
  }
]