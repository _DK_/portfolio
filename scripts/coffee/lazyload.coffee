import * as $ from "./helper"

export lazyload = () ->
  elements = $.getByClass('lazyload')
  if elements? and elements.length?
    for e in elements
      src = e.getAttribute('lazyload')
      if src?
        if (e.tagName == "LINK")
          e.setAttribute('href', src)
        else
          e.setAttribute('src', src)
        e.style.opacity = 1
        e.removeAttribute('lazyload')