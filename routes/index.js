var express = require('express');
var router = express.Router();
var aws = require('aws-sdk');

aws.config.update({ region: 'us-east-1' });

let techList = {
  "HTML5": ["https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5"],
  "Pug": ["https://pugjs.org/api/getting-started.html"],
  "CSS": ["https://developer.mozilla.org/en-US/docs/Web/CSS"],
  "SASS": ["http://sass-lang.com/"],
  "SCSS": ["http://sass-lang.com/"],
  "Stylus": ["http://stylus-lang.com/"],
  "JavaScript": ["https://developer.mozilla.org/en-US/docs/Web/javascript"],
  "CoffeeScript": ["http://coffeescript.org/"],
  "jQuery": ["https://jquery.com/"],
  "AngularJS": ["https://angularjs.org/"],
  "Jasmine": ["https://jasmine.github.io/"],
  "PHP": ["https://secure.php.net/"],
  "Laravel": ["https://laravel.com/"],
  "WordPress": ["https://wordpress.org/"],
  "Magento": ["https://magento.com/"],
  "Node.js": ["https://nodejs.org/"],
  "Express.js": ["https://expressjs.com/"],
  "Ruby": ["https://www.ruby-lang.org/"],
  "Ruby on Rails": ["http://rubyonrails.org/"],
  "MySQL": ["https://www.mysql.com/"],
  "Grunt": ["https://gruntjs.com/"],
  "Gulp": ["https://gulpjs.com/"],
  "Webpack": ["https://webpack.js.org/"],
  "Bootstrap": ["https://getbootstrap.com/"],
  "JSON": ["https://json.org/"],
  "XML": ["https://www.w3.org/TR/REC-xml/"],
  "Python": ["https://www.python.org/"],
  "Java": ["https://www.oracle.com/java/index.html"],
  "Groovy": ["http://www.groovy-lang.org/"],
  "TestNG-JUnit": ["http://testng.org/doc/"],
  "C": ["https://en.wikipedia.org/wiki/C_%28programming_language%29"],
  "C++": ["http://www.cplusplus.com/"],
  "Assembly": ["https://en.wikipedia.org/wiki/Assembly_language"],
  "Git": ["https://git-scm.com/"],
  "SVN": ["https://subversion.apache.org/"],
  "GitLab": ["https://about.gitlab.com/"],
  "Bitbucket": ["https://bitbucket.org/"],
  "CodebaseHQ": ["https://www.codebasehq.com/"],
  "Jira": ["https://www.atlassian.com/software/jira"],
  "Bash": ["https://www.gnu.org/software/bash/"],
  "PowerShell": ["https://docs.microsoft.com/en-us/powershell/"],
  "Ubuntu": ["https://www.ubuntu.com/"],
  "Linux": ["https://en.wikipedia.org/wiki/Linux"],
  "Windows Server": ["http://windowsserver.com/"],
  "AWS": ["https://aws.amazon.com/"],
  "Azure": ["https://azure.microsoft.com/"],
  "Microsoft Office": ["https://office.com/"],
  "Photoshop": ["https://www.photoshop.com/"],
  "Maven": ["https://maven.apache.org/"],
  "React Native": ["https://facebook.github.io/react-native/"],
  "React": ["https://reactjs.org/"]
};

let techPlanto = ["React Native", "React", "Java", "Bash", "Git", "Jira", "Bitbucket", "Maven"];
let techSophos = ["HTML5", "SCSS", "Bootstrap", "AngularJS", "Jasmine", "Groovy", "Java", "TestNG-JUnit", "JSON", "XML", "Bash", "PowerShell", "Git", "Jira", "Bitbucket", "AWS", "Azure", "Linux", "Microsoft Office"];
let techUnitemps = ["HTML5", "SASS", "PHP", "Laravel", "Git", "GitLab", "Python", "Webpack"];
let techZeta = ["PHP", "HTML5", "SASS", "CSS", "CodebaseHQ", "JavaScript", "jQuery", "Bootstrap", "Gulp", "Grunt", "Webpack", "Bash", "SVN", "WordPress", "Magento", "MySQL", "JSON", "XML", "Photoshop", "Ubuntu", "Linux", "Microsoft Office"];
let techBagwell = ["PowerShell", "Windows Server"];
// let techUptonglen = ["PHP", "HTML5", "CSS", "MySQL", "JavaScript", "jQuery"];

let techProjectZeta = ["PHP", "HTML5", "CSS", "CodebaseHQ", "JavaScript", "jQuery", "Bootstrap", "Grunt", "Bash", "SVN", "WordPress", "Photoshop", "Ubuntu", "Linux", "Microsoft Office"];
let techProjectHta = ["PHP", "HTML5", "SASS", "CodebaseHQ", "JavaScript", "jQuery", "Bootstrap", "Gulp", "Webpack", "Bash", "SVN", "WordPress", "Photoshop", "Ubuntu", "Linux", "Microsoft Office"];
let techProjectEcc = ["Java", "Git", "Ubuntu", "Linux", "Microsoft Office"];
let techProjectModellingCkd = ["Laravel", "PHP", "Python", "MatLab", "HTML5", "SASS", "Bootstrap", "Artisan", "Git", "GitLab", "Bash", "JavaScript", "jQuery"];

let data = {
  title: 'David Kennedy - Tech Enthusiast',
  techList: techList,
  employers: {
    planto: {
      name: 'Planto',
      jobTitle: 'Software Engineering Intern',
      duration: '2 Months',
      tech: techPlanto,
      id: 'planto'
    },
    sophos: {
      name: 'Sophos',
      jobTitle: 'Software Engineering Intern',
      duration: '1 Year',
      tech: techSophos,
      id: 'sophos'
    },
    unitemps: {
      name: 'Unitemps',
      jobTitle: 'Web API Developer',
      duration: '2 Months',
      tech: techUnitemps,
      id: 'unitemps'
    },
    zeta: {
      name: 'Zeta Agency',
      jobTitle: 'Web Developer',
      duration: '14 Months',
      tech: techZeta,
      id: 'zeta'
    },
    bagwell: {
      name: 'Bagwell Farm',
      jobTitle: 'Seasonal Summer Assistant',
      duration: '1 Year',
      tech: techBagwell,
      id: 'bagwell'
    },
    // uptonglen: {
    //   name: 'Upton Glen',
    //   jobTitle: 'Web Developer',
    //   duration: '3 Months',
    //   tech: techUptonglen,
    //   id: 'uptonglen'
    // }
  },
  educators: {
    uos: {
      name: 'University of Surrey',
      course: 'Computer Science Bsc (Hons)',
      result: 'Predicted grade: 1:1',
      id: "uos"
    },
    bpc: {
      name: 'Bournemouth and Poole College',
      course: 'Web and Games Development',
      result: 'Triple Distinction* (~460 UCAS points)',
      id: "bpc"
    },
    ths: {
      name: 'Thomas Hardye School',
      course: 'GCSEs',
      result: '8 GCSEs at grade A-C (inc. Maths and English)',
      id: "ths"
    },
  },
  projects: {
    zeta: {
      name: 'Zeta Website',
      tech: techProjectZeta,
      id: 'projectZeta'
    },
    hta: {
      name: 'Hampshire\'s Top Attractions',
      tech: techProjectHta,
      id: 'projectHta'
    },
    ecc: {
      name: 'Encrypted Chat Client',
      tech: techProjectEcc,
      id: 'projectEcc'
    },
    modellingckd: {
      name: 'SAKIDA',
      tech: techProjectModellingCkd,
      id: 'projectModellingCkd'
    }
  },
  emailSent: 'false'
};

router.get('/', function(req, res, next) {
  res.render('index', data);
  data.emailSent = 'false';
});

router.post('/contact', function(req, res, next) {
  if (req.body.name != "" && req.body.message != "" && req.body.email != "") {
    new aws.Lambda().invoke({
      FunctionName: 'portfolioContact',
      Payload: JSON.stringify([req.body.name, req.body.message, req.body.email, req.headers['x-forwarded-for'] + " - " + req.ips])
    }, function(err, data) {
      if (err) console.log(err, err.stack);
      else console.log(data);
    });
    data.emailSent = 'Thanks for getting in touch!';
  }
  else {
    data.emailSent = 'Error sending email! Please try again!';
  }
  res.redirect('/');
});

module.exports = router;
