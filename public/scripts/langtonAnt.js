let grid;
let x;
let y;
let dir;

let ANTUP = 0;
let ANTRIGHT = 1;
let ANTDOWN = 2;
let ANTLEFT = 3;


function setup() {
	createCanvas(window.innerWidth, window.innerHeight);
	pixelDensity(5);
	grid = make2DArray(width, height);
	x = width / 2;
	y = height / 2;
	dir = ANTLEFT
}

function turnRight() {
	dir = (dir + 1 > ANTLEFT) ? ANTUP : ++dir;
}

function turnLeft() {
	dir = (dir - 1 < ANTUP) ? ANTLEFT : --dir;
}

function moveForward() {
	switch(dir) {
		case ANTUP:
			y--;
			break;
		case ANTRIGHT:
			x++;
			break;
		case ANTDOWN:
			y++;
			break;
		case ANTLEFT:
			x--;
			break;
	}

	if (x > width - 1) {
		x = 0;
	} else if (x < 0) {
		x = width - 1;
	}

	if (y > height - 1) {
		y = 0;
	} else if (y < 0) {
		y = height - 1;
	}
}

function draw() {
	strokeWeight(1);

	for (let n = 0; n < 100; ++n) {
		let state = grid[x][y];
		if (state == 0) {
			turnRight();
			grid[x][y] = 1;
		} else if (state == 1) {
			turnLeft();
			grid[x][y] = 0;
		}

		stroke((grid[x][y] == 1) ? color(0) : color(255));
		point(x, y);
		moveForward();
	}
}

function make2DArray(cols, rows) {
	let arr = new Array(cols);
	for (let i = 0; i < arr.length; ++i) {
		arr[i] = new Array(rows);
		for (let j = 0; j < arr[i].length; ++j) {
			arr[i][j] = 0;
		}
	}
	return arr;
}