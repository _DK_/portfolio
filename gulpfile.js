var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');
var sourcemaps = require('gulp-sourcemaps');
var livereload = require('gulp-livereload');

gulp.task('build', function() {
    return browserify({ entries: './scripts/script.js', debug: true })
        .transform("babelify", { presets: ["es2015"] })
        .bundle()
        .pipe(source('script.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init())
        //.pipe(uglify())
        .pipe(sourcemaps.write('./maps'))
        .pipe(gulp.dest('./public/scripts/'))
        .pipe(livereload());
});

gulp.task('watch', gulp.series('build', function() {
    livereload.listen();
    gulp.watch('./scripts/*.js', gulp.series('build'));
}));

gulp.task('default', gulp.series('build', 'watch'));
